#include <iostream>
#include <functional>
#include <string>
#include <vector>
#include <algorithm>
#include <tuple>
#include <map>

using namespace std;

struct Result
{
    int right = 0;
    int close = 0;
    int wrong = 0;
};

bool operator==(const Result& lhs, const Result& rhs)
{
    return lhs.right == rhs.right && lhs.close == rhs.close && lhs.wrong == rhs.wrong;
}

bool operator!=(const Result& lhs, const Result& rhs)
{
    return !(lhs == rhs);
}

bool operator<(const Result& lhs, const Result& rhs)
{
    return std::tie(lhs.right, lhs.close, lhs.wrong) < std::tie(rhs.right, rhs.close, rhs.wrong);
}

ostream& operator<<(ostream& out, const Result& res)
{
    return out << string(res.right, '*') << string(res.close, 'P') << string(res.wrong, 'X');
}

template<typename T>
ostream& operator<<(ostream& out, const vector<T>& vec)
{
	out << "{";
	bool first = true;
	for (const auto& item : vec)
	{
		if (!first)
			out << ", ";
		first = false;
		out << item;
	}
	return out << "}";
}

bool done(const Result& res)
{
    return res.close == 0 && res.wrong == 0;
}

class Code
{
public:
    Code(string code)
        : code(code)
    {}

    Result operator()(string check) const
    {
        Result res;
        string check2;
        string code2;
        for (size_t i = 0; i < code.size(); ++i) {
            if (code[i] == check[i])
                ++res.right;
            else {
                check2 += check[i];
                code2 += code[i];
            }
        }
        for (auto c : check2) {
            size_t pos = code2.find_first_of(c);
            if (pos != string::npos) {
                ++res.close;
                code2.erase(pos, 1);
            }
        }
        res.wrong = code2.length();
        return res;
    }

    string code;
};

ostream& operator<<(ostream& out, const Code& code)
{
	return out << code.code;
}

vector<size_t> idxCode(size_t value, size_t digits, size_t figures)
{
    vector<size_t> res(digits);
    for (auto& v : res) {
        v = value % figures;
        value /= figures;
    }
    return res;
}

string strCode(size_t value, size_t digits, string figures)
{
    auto vec = idxCode(value, digits, figures.length());
    string res;
    for (auto v : vec)
        res.push_back(figures[v]);
    return res;
}

vector<string> allChoices(size_t digits, string figures)
{
    size_t max = pow(figures.length(), digits);
    vector<string> res;
    for (size_t i = 0; i < max; ++i)
        res.push_back(strCode(i, digits, figures));
    return res;
}

Code makeCode(size_t value, size_t digits, string figures)
{
    return Code{strCode(value, digits, figures)};
}

vector<Code> allCodes(size_t digits, string figures)
{
    size_t max = pow(figures.length(), digits);
    vector<Code> res;
    for (size_t i = 0; i < max; ++i)
        res.push_back(makeCode(i, digits, figures));
    return res;
}

float worstPotential(const vector<Code>& codes, string choice)
{
    map<Result, size_t> potential;
    for (const auto& code : codes) {
        ++potential[code(choice)];
    }
    size_t worst = 0;
	bool couldEnd = false;
    for (const auto& pair : potential) {
        worst = max(pair.second, worst);
		if (done(pair.first))
			couldEnd = true;
    }
    return worst + (couldEnd ? -.1f : 0.f);
}

Result lastRes;
std::map<Result, string> checks;

int solve(std::function<Result(string)> check, vector<Code> possible, const vector<string>& choices)
{
    //auto possible = allCodes(digits, figures);
    //auto choices = allChoices(digits, figures);
    for (int steps = 1; true; ++steps) {
        float bestWorst = possible.size();
        string bestChoice;
		vector<string> equivalents;
		int opt = 0;
        for (const auto& choice : choices) {
            float worst = worstPotential(possible, choice);
            if (worst < bestWorst) {
                bestWorst = worst;
                bestChoice = choice;
				equivalents = {choice};
            }
			else if (worst == bestWorst)
				equivalents.push_back(choice);
			//if (++opt % 1000 == 0)
			//	cout << ((opt * 100) / choices.size()) << "%" << endl;
        }

        auto res = check(bestChoice);
		if (steps == 2)
			checks[lastRes] = bestChoice;
        cout << "choice " << steps << ": " << bestChoice << "(with " << equivalents.size() << " equivalents)" << "  " << res << endl;
        if (done(res)) {
            cout << "completed in " << steps << " guesses!" << endl << endl;
            return steps;
        }
		if (steps == 1)
			lastRes = res;
        auto it = std::remove_if(possible.begin(), possible.end(), [bestChoice, res](const Code& code)
        {
            return code(bestChoice) != res;
        });
        possible.erase(it, possible.end());
		//cout << "possibilities reduced to " << possible << endl;
    }
}

int main()
{
	int digits;
	std::string figures;

	std::cout << "Number of digits in code? ";
	std::cin >> digits;
	std::cout << "Possible figures (character string, no spaces)? ";
	std::cin >> figures;

	auto codes = allCodes(digits, figures);
	auto choices = allChoices(digits, figures);

	solve([](string test)
	{
	cout << "result of (" << test << ")" << endl;
	Result res;
	cout << "right? ";
	cin >> res.right;
	cout << "close? ";
	cin >> res.close;
	res.wrong = test.length() - (res.right + res.close);
	return res;
	}, codes, choices);

	//int worst = 0;
	//Code worstCode{"111"};
	//for (const auto& code : codes)
	//{
	//	int sol = solve(code, codes, choices);
	//	if (sol > worst) {
	//		worst = sol;
	//		worstCode = code;
	//	}
	//}

	//cout << "=====================================" << endl << endl;

	////solve(worstCode, digits, figures);

	//for (const auto& pair : checks)
	//{
	//	cout << pair.first << ' ' << pair.second << endl;
	//}

	system("pause");

    return 0;
}
